package br.com.concretsolution.githubjavapop.views

import android.annotation.SuppressLint
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import br.com.concretsolution.githubjavapop.R
import br.com.concretsolution.githubjavapop.controllers.GithubService
import br.com.concretsolution.githubjavapop.controllers.PullRequestAdapter
import br.com.concretsolution.githubjavapop.models.PullRequest
import kotlinx.android.synthetic.main.activity_pull_request.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

@SuppressLint("Registered")
@EActivity(R.layout.activity_pull_request)
open class PullRequestActivity : AppCompatActivity() {

    private var uri: Uri? = null

    @AfterViews
    protected fun init() {
        uri = intent.data
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.title = intent.extras.getString("repository_name")
    }

    @AfterViews
    protected fun load() {
       Observable.fromCallable(GithubService().getPullRequestsObservable(uri!!))
               .subscribeOn(Schedulers.newThread())
               .observeOn(AndroidSchedulers.mainThread())
               .doOnSubscribe(showProgressBar())
               .subscribe(showList(), showError(), hideProgressBar())
    }

    private fun showProgressBar(): () -> Unit {
        return {
            progressBar2.visibility = View.VISIBLE
        }
    }

    private fun showList(): (List<PullRequest>?) -> Unit = {
        showHeader(it!!)
        var pullRequestListAdapter = PullRequestAdapter(this, it!!)
        pullRequestRecyclerView.adapter = pullRequestListAdapter
        pullRequestRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        pullRequestRecyclerView.adapter.notifyDataSetChanged()
    }

    private fun showHeader(pullRequests: List<PullRequest>) {
        var count = pullRequests.filter { it.state.equals("open") }.count()
        txtOpened.text = "$count opened "
        count = pullRequests.filter { !it.state.equals("open") }.count()
        txtClosed.text = " / $count closed "

    }

    private fun showError(): (Throwable) -> Unit =
            { Toast.makeText(this, it.message, Toast.LENGTH_LONG).show() }

    private fun hideProgressBar(): () -> Unit {
        return {
            progressBar2.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId) {
            android.R.id.home -> finish()
        }
        return true

    }

}
