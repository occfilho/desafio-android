package br.com.concretsolution.githubjavapop.controllers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.concretsolution.githubjavapop.R
import br.com.concretsolution.githubjavapop.models.PullRequest
import com.squareup.picasso.Picasso
import org.joda.time.DateTime
import java.text.DateFormat
import java.util.*


/**
 * Created by oliveirafilho on 12/11/17.
 */
class PullRequestAdapter(private val context: Context, private val pullRequests: List<PullRequest>) : RecyclerView.Adapter<PullRequestViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PullRequestViewHolder {
        return PullRequestViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list_pull_request, null))
    }

    override fun onBindViewHolder(holder: PullRequestViewHolder?, position: Int) {

        holder?.txtPullRequestName?.text = pullRequests[position].title
        holder?.txtPullRequestData?.text = localDateFormat(pullRequests[position].createdAt)
        holder?.txtDescription?.text = pullRequests[position].body
        holder?.txtUserName?.text = pullRequests[position].user.login
        holder?.txtFullName?.text = pullRequests[position].authorAssociation

        Picasso.with(context)
                .load(pullRequests[position].user.avatarUrl)
                .resize(90, 90)
                .centerCrop()
                .into(holder?.pictureCircleImageView)

        holder?.itemView?.tag = pullRequests[position]

        holder?.itemView?.setOnClickListener({
            var pr = it.tag
            if (pr is PullRequest) {
                openBrowser(pr.htmlUrl)
            }
        })

    }

    override fun getItemCount(): Int {
        return pullRequests.size
    }

    private fun localDateFormat(data: String): String {
        return if (data.isNotEmpty()) DateFormat.getDateInstance(DateFormat.SHORT,
                Locale.getDefault()).format(DateTime.parse(data).toDate())
        else ""
    }

    private fun openBrowser(url: String) {
        if (url.isNotEmpty()) {
            var browser = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(browser)
        }
    }


}