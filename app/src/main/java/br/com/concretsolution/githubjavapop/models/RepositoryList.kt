package br.com.concretsolution.githubjavapop.models

import java.util.HashMap
import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("total_count", "incomplete_results", "items")
class RepositoryList {

    @JsonProperty("total_count")
    val totalCount: Int? = null
    @JsonProperty("incomplete_results")
    val incompleteResults: Boolean? = null
    @JsonProperty("items")
    val items: List<Item>? = null
    @JsonIgnore
    val additionalProperties: Map<String, Any> = HashMap()


}
