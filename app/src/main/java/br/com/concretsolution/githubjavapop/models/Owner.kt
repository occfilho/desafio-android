package br.com.concretsolution.githubjavapop.models

import java.util.HashMap
import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("login", "id", "avatar_url", "gravatar_id", "url", "html_url", "followers_url", "following_url", "gists_url", "starred_url", "subscriptions_url", "organizations_url", "repos_url", "events_url", "received_events_url", "type", "site_admin")
class Owner {

    @JsonProperty("login")
    val login: String? = null
    @JsonProperty("id")
    val id: Int? = null
    @JsonProperty("avatar_url")
    val avatarUrl: String? = null
    @JsonProperty("gravatar_id")
    val gravatarId: String? = null
    @JsonProperty("url")
    val url: String? = null
    @JsonProperty("html_url")
    val htmlUrl: String? = null
    @JsonProperty("followers_url")
    val followersUrl: String? = null
    @JsonProperty("following_url")
    val followingUrl: String? = null
    @JsonProperty("gists_url")
    val gistsUrl: String? = null
    @JsonProperty("starred_url")
    val starredUrl: String? = null
    @JsonProperty("subscriptions_url")
    val subscriptionsUrl: String? = null
    @JsonProperty("organizations_url")
    val organizationsUrl: String? = null
    @JsonProperty("repos_url")
    val reposUrl: String? = null
    @JsonProperty("events_url")
    val eventsUrl: String? = null
    @JsonProperty("received_events_url")
    val receivedEventsUrl: String? = null
    @JsonProperty("type")
    val type: String? = null
    @JsonProperty("site_admin")
    val siteAdmin: Boolean? = null
    @JsonIgnore
    val additionalProperties: Map<String, Any> = HashMap()

}
