package br.com.concretsolution.githubjavapop.controllers

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.concretsolution.githubjavapop.R
import br.com.concretsolution.githubjavapop.models.Item
import com.squareup.picasso.Picasso


/**
 * Created by oliveirafilho on 09/11/17.
 */
class RepositoryAdapter(private val context: Context, private var items: List<Item>?, private val onClickListener: View.OnClickListener) : RecyclerView.Adapter<RepositoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoryViewHolder =
            RepositoryViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list_repository, null), onClickListener)

    override fun onBindViewHolder(holder: RepositoryViewHolder?, position: Int) {
        holder?.txtTitleTepository?.text = items!![position].name
        holder?.txtDescricao?.text = items!![position].description
        holder?.txtStar?.text = items!![position].stargazersCount.toString()
        holder?.txtFork?.text = items!![position].forksCount.toString()
        holder?.txtUserName?.text = items!![position].owner?.login
        holder?.txtFullName?.text = items!![position].fullName
        Picasso.with(context)
                .load(items!![position].owner?.avatarUrl)
                .resize(90, 90)
                .centerCrop()
                .into(holder?.imgPicture)
        holder?.itemView?.tag = items!![position]
    }

    override fun getItemCount(): Int = items!!.size

    fun additems(its: List<Item>) {
        items = items?.plus(its)
    }

}