package br.com.concretsolution.githubjavapop.controllers

import android.net.Uri
import br.com.concretsolution.githubjavapop.models.Item
import br.com.concretsolution.githubjavapop.models.PullRequest
import br.com.concretsolution.githubjavapop.models.RepositoryList
import org.springframework.http.MediaType
import org.springframework.http.converter.AbstractHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import java.util.concurrent.Callable

/**
 * Created by oliveirafilho on 10/11/17.
 */
class GithubService {

    private val supportedMediaTypes = listOf(MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON)

    fun getRepositoriesObservable(page: Int): Callable<List<Item>?> {
        return Callable {
            getRepositories(page)
        }
    }

    fun getPullRequestsObservable(uri: Uri): Callable<List<PullRequest>?> {
        return Callable {
            getPullRequets(uri)
        }
    }

    private fun getRepositories(page: Int): List<Item>? {
        var restTemplate = RestTemplate()
        restTemplate.messageConverters = createMessageConverter()
        var response = restTemplate.getForEntity("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=$page", RepositoryList::class.java)
        return response.body?.items.orEmpty()
    }

    private fun getPullRequets(uri: Uri): List<PullRequest>? {
        var restTemplate = RestTemplate()
        restTemplate.messageConverters = createMessageConverter()
        var response = restTemplate.getForEntity(uri.toString(), Array<PullRequest>::class.java)
        return response.body?.asList()
    }

    private fun createMessageConverter(): List<AbstractHttpMessageConverter<Any>> {
        var mappingMessageConverter = MappingJackson2HttpMessageConverter()
        mappingMessageConverter.supportedMediaTypes = supportedMediaTypes
        return listOf(mappingMessageConverter)
    }

}

