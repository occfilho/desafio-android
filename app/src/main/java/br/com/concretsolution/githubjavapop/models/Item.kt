package br.com.concretsolution.githubjavapop.models

import java.util.HashMap
import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("id", "name", "full_name", "owner", "private", "html_url", "description", "fork", "url", "forks_url", "keys_url", "collaborators_url", "teams_url", "hooks_url", "issue_events_url", "events_url", "assignees_url", "branches_url", "tags_url", "blobs_url", "git_tags_url", "git_refs_url", "trees_url", "statuses_url", "languages_url", "stargazers_url", "contributors_url", "subscribers_url", "subscription_url", "commits_url", "git_commits_url", "comments_url", "issue_comment_url", "contents_url", "compare_url", "merges_url", "archive_url", "downloads_url", "issues_url", "pulls_url", "milestones_url", "notifications_url", "labels_url", "releases_url", "deployments_url", "created_at", "updated_at", "pushed_at", "git_url", "ssh_url", "clone_url", "svn_url", "homepage", "size", "stargazers_count", "watchers_count", "language", "has_issues", "has_projects", "has_downloads", "has_wiki", "has_pages", "forks_count", "mirror_url", "archived", "open_issues_count", "forks", "open_issues", "watchers", "default_branch", "score")
class Item {

    @JsonProperty("id")
    val id: Int? = null
    @JsonProperty("name")
    val name: String? = null
    @JsonProperty("full_name")
    val fullName: String? = null
    @JsonProperty("owner")
    val owner: Owner? = null
    @JsonProperty("private")
    val _private: Boolean? = null
    @JsonProperty("html_url")
    val htmlUrl: String? = null
    @JsonProperty("description")
    val description: String? = null
    @JsonProperty("fork")
    val fork: Boolean? = null
    @JsonProperty("url")
    val url: String? = null
    @JsonProperty("forks_url")
    val forksUrl: String? = null
    @JsonProperty("keys_url")
    val keysUrl: String? = null
    @JsonProperty("collaborators_url")
    val collaboratorsUrl: String? = null
    @JsonProperty("teams_url")
    val teamsUrl: String? = null
    @JsonProperty("hooks_url")
    val hooksUrl: String? = null
    @JsonProperty("issue_events_url")
    val issueEventsUrl: String? = null
    @JsonProperty("events_url")
    val eventsUrl: String? = null
    @JsonProperty("assignees_url")
    val assigneesUrl: String? = null
    @JsonProperty("branches_url")
    val branchesUrl: String? = null
    @JsonProperty("tags_url")
    val tagsUrl: String? = null
    @JsonProperty("blobs_url")
    val blobsUrl: String? = null
    @JsonProperty("git_tags_url")
    val gitTagsUrl: String? = null
    @JsonProperty("git_refs_url")
    val gitRefsUrl: String? = null
    @JsonProperty("trees_url")
    val treesUrl: String? = null
    @JsonProperty("statuses_url")
    val statusesUrl: String? = null
    @JsonProperty("languages_url")
    val languagesUrl: String? = null
    @JsonProperty("stargazers_url")
    val stargazersUrl: String? = null
    @JsonProperty("contributors_url")
    val contributorsUrl: String? = null
    @JsonProperty("subscribers_url")
    val subscribersUrl: String? = null
    @JsonProperty("subscription_url")
    val subscriptionUrl: String? = null
    @JsonProperty("commits_url")
    val commitsUrl: String? = null
    @JsonProperty("git_commits_url")
    val gitCommitsUrl: String? = null
    @JsonProperty("comments_url")
    val commentsUrl: String? = null
    @JsonProperty("issue_comment_url")
    val issueCommentUrl: String? = null
    @JsonProperty("contents_url")
    val contentsUrl: String? = null
    @JsonProperty("compare_url")
    val compareUrl: String? = null
    @JsonProperty("merges_url")
    val mergesUrl: String? = null
    @JsonProperty("archive_url")
    val archiveUrl: String? = null
    @JsonProperty("downloads_url")
    val downloadsUrl: String? = null
    @JsonProperty("issues_url")
    val issuesUrl: String? = null
    @JsonProperty("pulls_url")
    val pullsUrl: String? = null
    @JsonProperty("milestones_url")
    val milestonesUrl: String? = null
    @JsonProperty("notifications_url")
    val notificationsUrl: String? = null
    @JsonProperty("labels_url")
    val labelsUrl: String? = null
    @JsonProperty("releases_url")
    val releasesUrl: String? = null
    @JsonProperty("deployments_url")
    val deploymentsUrl: String? = null
    @JsonProperty("created_at")
    val createdAt: String? = null
    @JsonProperty("updated_at")
    val updatedAt: String? = null
    @JsonProperty("pushed_at")
    val pushedAt: String? = null
    @JsonProperty("git_url")
    val gitUrl: String? = null
    @JsonProperty("ssh_url")
    val sshUrl: String? = null
    @JsonProperty("clone_url")
    val cloneUrl: String? = null
    @JsonProperty("svn_url")
    val svnUrl: String? = null
    @JsonProperty("homepage")
    val homepage: String? = null
    @JsonProperty("size")
    val size: Int? = null
    @JsonProperty("stargazers_count")
    val stargazersCount: Int? = null
    @JsonProperty("watchers_count")
    val watchersCount: Int? = null
    @JsonProperty("language")
    val language: String? = null
    @JsonProperty("has_issues")
    val hasIssues: Boolean? = null
    @JsonProperty("has_projects")
    val hasProjects: Boolean? = null
    @JsonProperty("has_downloads")
    val hasDownloads: Boolean? = null
    @JsonProperty("has_wiki")
    val hasWiki: Boolean? = null
    @JsonProperty("has_pages")
    val hasPages: Boolean? = null
    @JsonProperty("forks_count")
    val forksCount: Int? = null
    @JsonProperty("mirror_url")
    val mirrorUrl: Any? = null
    @JsonProperty("archived")
    val archived: Boolean? = null
    @JsonProperty("open_issues_count")
    val openIssuesCount: Int? = null
    @JsonProperty("forks")
    val forks: Int? = null
    @JsonProperty("open_issues")
    val openIssues: Int? = null
    @JsonProperty("watchers")
    val watchers: Int? = null
    @JsonProperty("default_branch")
    val defaultBranch: String? = null
    @JsonProperty("score")
    val score: Double? = null
    @JsonIgnore
    val additionalProperties: Map<String, Any> = HashMap()

}
