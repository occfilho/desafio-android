package br.com.concretsolution.githubjavapop.controllers

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.item_list_pull_request.view.*

/**
 * Created by oliveirafilho on 12/11/17.
 */
class PullRequestViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val txtPullRequestName     = itemView.txtPullRequestTitle
    val txtPullRequestData     = itemView.txtPullRequestData
    val txtDescription         = itemView.txtDescription
    val pictureCircleImageView = itemView.pictureCircleImageView
    val txtUserName            = itemView.txtUserName
    val txtFullName            = itemView.txtFullName

}