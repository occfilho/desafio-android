package br.com.concretsolution.githubjavapop.controllers

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import kotlinx.android.synthetic.main.item_list_repository.view.*

/**
 * Created by oliveirafilho on 10/11/17.
 */
class RepositoryViewHolder(itemview: View, onClickListener: View.OnClickListener): ViewHolder(itemview) {

    val txtTitleTepository = itemview.txtTitleTepository
    val txtDescricao       = itemview.txtDescricao
    val simbolFork         = itemview.simbolFork
    val txtFork            = itemview.txtFork
    val simbolStar         = itemview.simbolStar
    val txtStar            = itemview.txtStar
    val imgPicture         = itemview.imgPicture
    val txtUserName        = itemview.txtUserName
    val txtFullName        = itemview.txtFullName

    init {
        simbolFork.typeface = FontManager().getTypeface(itemview.context)
        simbolStar.typeface = FontManager().getTypeface(itemview.context)
        itemview.setOnClickListener(onClickListener)
    }

}