package br.com.concretsolution.githubjavapop.views

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import br.com.concretsolution.githubjavapop.R
import br.com.concretsolution.githubjavapop.controllers.GithubService
import br.com.concretsolution.githubjavapop.controllers.RepositoryAdapter
import br.com.concretsolution.githubjavapop.models.Item
import kotlinx.android.synthetic.main.activity_repository.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

@SuppressLint("Registered")
@EActivity(R.layout.activity_repository)
open class MainRepositoryActivity : AppCompatActivity() {

    private var page: Int = 0

    @AfterViews
    protected fun load() {
        Observable.fromCallable(GithubService().getRepositoriesObservable(++page))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(showProgressBar())
                .subscribe(showList(), showError(), hideProgressBar())
    }

    private fun showProgressBar(): () -> Unit {
        return {
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun showList(): (List<Item>?) -> Unit = {
        when (repositoryRecyclerView?.adapter) {
            null -> addList(it)
            else -> addMoreList(it)
        }
    }

    private fun showError(): (Throwable) -> Unit = {
        Log.e("Error", it.message)
        Toast.makeText(this,
                getString(R.string.nao_foi_possivel_carregar_dados_do_github), Toast.LENGTH_LONG).show()
        hideProgressBar()()
    }


    private fun hideProgressBar(): () -> Unit {
        return {
            progressBar.visibility = View.GONE
        }
    }


    private fun createOnScrollListener(): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    if (!recyclerView!!.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                        load()
                    }
                }
            }
        }
    }

    private fun addList(it: List<Item>?) {
        repositoryRecyclerView.adapter = RepositoryAdapter(this, it, showPullRequests())
        repositoryRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        repositoryRecyclerView.adapter.notifyDataSetChanged()
        repositoryRecyclerView.addOnScrollListener(createOnScrollListener())
    }

    private fun addMoreList(it: List<Item>?) {
        val adapter = repositoryRecyclerView.adapter
        if (adapter is RepositoryAdapter) {
            adapter.additems(it!!)
            adapter.notifyDataSetChanged()
        }

    }

    private fun showPullRequests() = View.OnClickListener {
        val pullRequestActivity = Intent(this, PullRequestActivity_::class.java)
        val item = it.tag
        if (item is Item) {
            pullRequestActivity.data = Uri.parse(item.pullsUrl?.substring(0, item.pullsUrl.indexOf("pulls").plus("pulls".length))
                    ?: "")
            pullRequestActivity.putExtra("repository_name", item.name)
            startActivity(pullRequestActivity)
        }

    }

}
