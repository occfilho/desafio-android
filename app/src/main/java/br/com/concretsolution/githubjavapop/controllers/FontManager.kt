package br.com.concretsolution.githubjavapop.controllers

import android.graphics.Typeface
import android.view.View
import android.R.attr.typeface
import android.content.Context
import android.widget.TextView
import android.view.ViewGroup


/**
 * Created by oliveirafilho on 09/11/17.
 */

class FontManager {

    private val FONTAWESOME = "fontawesome-webfont.ttf"

    fun getTypeface(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, FONTAWESOME)
    }

}
