package br.com.concretsolution.githubjavapop;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.concretsolution.githubjavapop.views.MainRepositoryActivity_;
import br.com.concretsolution.githubjavapop.views.PullRequestActivity_;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by oliveirafilho on 15/11/17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class GithubjavaPopTest {

    @Rule
    public ActivityTestRule<MainRepositoryActivity_> mainRepositoryActivity_activityTestRule
            = new ActivityTestRule<>(MainRepositoryActivity_.class);

    @Test
    public void scrollAndBrowseBetweenIntents() throws InterruptedException {
        Thread.sleep(10000);
        onView(withId(R.id.repositoryRecyclerView)).check(matches(isDisplayed()));
        onView(withId(R.id.repositoryRecyclerView)).perform(swipeUp());
        Thread.sleep(1000);
        onView(withId(R.id.repositoryRecyclerView)).perform(swipeUp());
        Thread.sleep(1000);
        onView(withId(R.id.repositoryRecyclerView)).perform(swipeUp());
        Thread.sleep(1000);
        onView(withId(R.id.repositoryRecyclerView)).perform(RecyclerViewActions.scrollToPosition(3), click());
        Thread.sleep(1000);
        onView(withId(R.id.pullRequestRecyclerView)).check(matches(isDisplayed()));
        Thread.sleep(10000);
        onView(withId(R.id.pullRequestRecyclerView)).perform(click());
        Thread.sleep(10000);

    }
}
